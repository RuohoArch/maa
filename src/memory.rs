pub trait Unit: Sized + Copy {
    fn byte_length() -> usize;

    unsafe fn assign_slice(&mut self, data: &[u8]);

    unsafe fn as_slice(&self) -> &[u8];
    unsafe fn as_slice_mut(&mut self) -> &mut [u8];
}

macro_rules! unit_impl {
    ($size:literal) => {
        impl Unit for [u8; $size] {
            fn byte_length() -> usize {
                $size
            }

            unsafe fn assign_slice(&mut self, data: &[u8]) {
                let ptr = data.as_ptr() as *const Self;
                let val = std::ptr::read(ptr);

                *self = val;
            }

            unsafe fn as_slice(&self) -> &[u8] {
                let ptr = (self as *const Self) as *const u8;
                let len = std::mem::size_of::<Self>();

                std::slice::from_raw_parts(ptr, len)
            }

            unsafe fn as_slice_mut(&mut self) -> &mut [u8] {
                let ptr = (self as *mut Self) as *mut u8;
                let len = std::mem::size_of::<Self>();

                std::slice::from_raw_parts_mut(ptr, len)
            }
        }
    };
}

unit_impl!(1); // u8
unit_impl!(2); // u16
unit_impl!(4); // u32
unit_impl!(8); // u64
unit_impl!(16); // u128

pub trait MemoryAccess {
    type Addr;

    unsafe fn read<U: Unit>(&self, addr: Self::Addr, ret: &mut U);

    unsafe fn write<U: Unit>(&mut self, addr: Self::Addr, val: &U);
}
