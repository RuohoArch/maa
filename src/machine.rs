use crate::cpu::PhysicalCore;

const NUM_CPUS: usize = 16;

pub enum CpuInst {
    Halt,
}

pub struct Machine {
    pub cpus: [Box<PhysicalCore>; NUM_CPUS],

    code: Vec<u8>,

    pub halted: bool,

    /// append responses to the memory controller queue
    memory_receiver: crossbeam_channel::Receiver<crate::main_memory::Response>,
    /// receiver for replies from the memory controller
    memory_sender: crossbeam_channel::Sender<crate::main_memory::Request>,
    memory_thread: std::thread::JoinHandle<()>,

    // private implementation details
    actions: Vec<CpuInst>,
}

impl Machine {
    pub fn new(code: Vec<u8>) -> Self {
        let (sreq, rrep) = crossbeam_channel::unbounded();
        let (sres, rres) = crossbeam_channel::unbounded();

        let handle = std::thread::spawn(move || {
            crate::main_memory::thread_func(rrep, sres);
        });

        let mut this = Machine {
            cpus: [
                Box::new(PhysicalCore::new()),
                Box::new(PhysicalCore::new()),
                Box::new(PhysicalCore::new()),
                Box::new(PhysicalCore::new()),
                Box::new(PhysicalCore::new()),
                Box::new(PhysicalCore::new()),
                Box::new(PhysicalCore::new()),
                Box::new(PhysicalCore::new()),
                Box::new(PhysicalCore::new()),
                Box::new(PhysicalCore::new()),
                Box::new(PhysicalCore::new()),
                Box::new(PhysicalCore::new()),
                Box::new(PhysicalCore::new()),
                Box::new(PhysicalCore::new()),
                Box::new(PhysicalCore::new()),
                Box::new(PhysicalCore::new()),
            ],

            code,

            memory_receiver: rres,
            memory_sender: sreq,
            memory_thread: handle,

            halted: false,

            actions: Vec::with_capacity(NUM_CPUS),
        };

        this.cpus[0].state = crate::cpu::State::Running;
        this.cpus[0].process_states[0].state = crate::cpu::process::State::Ready;

        this
    }

    pub unsafe fn process(&mut self) {
        use rayon::prelude::IndexedParallelIterator;
        use rayon::prelude::IntoParallelRefMutIterator;
        use rayon::prelude::ParallelBridge;
        use rayon::prelude::ParallelExtend;
        use rayon::prelude::ParallelIterator;

        // if there is memory coming in, write it to the appropriate buffer
        if let Ok(resp) = self.memory_receiver.try_recv() {
            let cpu = resp.process.cpu();
            let proc = resp.process.procss();

            let proc = &mut self.cpus[cpu as usize].process_states[proc as usize];
            proc.state = crate::cpu::process::State::Ready;
            proc.mem_read_buffer = resp.data;
        }

        let sender = self.memory_sender.clone();
        let code = &self.code[..];

        let actions = self
            .cpus
            .iter_mut()
            .filter(|cpu| cpu.state == crate::cpu::State::Running)
            .enumerate()
            .par_bridge()
            .filter_map(|(id, cpu)| {
                let sender = sender.clone();
                cpu.process(id as u8, sender, code)
            });

        self.actions.par_extend(actions);

        for action in self.actions.drain(..) {
            match action {
                CpuInst::Halt => {
                    self.halted = true;
                }
                _ => {}
            }
        }
    }

    pub fn finish(self) {
        // this tells the memory thread to shut down
        drop(self.memory_sender);

        self.memory_thread
            .join()
            .expect("main memory thread panicked");
    }
}
