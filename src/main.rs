pub mod cpu;
pub mod main_memory;
pub mod memory;

pub mod machine;

use crate::machine::Machine;

static CODE: &[u8] = &[
    // loadi r1, r0, 42
    0b000001_00,
    0b001_00000,
    0b00000000,
    0b00101010,
    // "noop"
    // load r0, r0
    // load r0, r0, r0, 0, 0
    0b000000_00,
    0b000_00000,
    0b00000_000,
    0b00_000000,
    // load r1, r2
    // load r1, r2, r0, 0, 0
    0b000000_00,
    0b001_00010,
    0b00000_000,
    0b00_000000,
    // cpu r0, 0
    0b100001_00,
    0b000_00000,
    0b000000000,
    0b000000000,
];

fn main() {
    let mut machine = Machine::new(Vec::from(CODE));

    while !machine.halted {
        unsafe {
            machine.process();
        }
    }

    println!(
        "{:#?}",
        &machine.cpus[0].process_states[0].registers.regs[0][0]
    );

    machine.finish();
}
