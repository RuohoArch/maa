pub mod mmu;

use crate::cpu::process::ProcessId;

use self::mmu::{PhysicalAddress, VirtualAddress};

pub struct MainMemory {
    pub queue: Queue,
    pub mmu: mmu::MemoryManagementUnit,

    pub pages: Vec<Page>,
}

pub struct Queue {
    channel: crossbeam_channel::Receiver<Request>,
}

const MEMORY_LINE_SIZE: usize = 64;

pub type MemoryLine = [u8; MEMORY_LINE_SIZE];

const PAGE_SIZE: usize = 4 * 1024;

const MEMORY_LINES_PER_PAGE: usize = PAGE_SIZE / MEMORY_LINE_SIZE;

pub struct Page {
    pub content: [MemoryLine; MEMORY_LINES_PER_PAGE],
}

impl Page {
    pub fn new() -> Self {
        // using unsafe here is okay, since a two dimensional array of bytes is still just
        // a collection of bytes, meaning it's a valid values and cannot cause undefined
        // behavior

        Self {
            content: unsafe { std::mem::zeroed() },
        }
    }
}

pub fn thread_func(
    receiver: crossbeam_channel::Receiver<Request>,
    replies: crossbeam_channel::Sender<Response>,
) {
    loop {
        let req = match receiver.recv() {
            Ok(val) => val,
            // we are supposed to shut down :/
            Err(_) => {
                return;
            }
        };
    }
}

pub enum Request {
    Read {
        process: ProcessId,
        addr: VirtualAddress,
        unit_size: usize,
    },
    Write {
        process: ProcessId,
        addr: VirtualAddress,
        data: MemoryLine,
        unit_size: usize,
    },
}

pub struct Response {
    pub process: ProcessId,
    pub data: MemoryLine,
}
