pub mod process;

use self::process::ProcessState;
use crate::memory::MemoryAccess;

const NUM_PROCESSES: usize = 8;

pub struct PhysicalCore {
    pub state: State,

    // number of clock cycles needed until process can be inside "pipeline" again
    //
    // this virtual machine "pretends" to have a pipeline, while in reality,
    // the first process that enters the pipe will be processed completely!
    // in order to respect that only one process "executes" in the pipeline,
    // a list has to be maintained with clock times to wait until a process can be
    // executed again.
    // 0 means the process can be executed
    // 1 means the process can not be executed yet, one more cycle has to be performed
    pub process_proc_ages: [u8; NUM_PROCESSES],

    pub last_proc: u8,

    pub process_states: [ProcessState; NUM_PROCESSES],
}

impl PhysicalCore {
    pub fn new() -> Self {
        PhysicalCore {
            state: State::Paused,

            process_proc_ages: unsafe { std::mem::zeroed() },
            // say the "last process" was used last time initially, then the
            // "next" one to be considered will be the first
            last_proc: 7,

            process_states: [
                ProcessState::new(),
                ProcessState::new(),
                ProcessState::new(),
                ProcessState::new(),
                ProcessState::new(),
                ProcessState::new(),
                ProcessState::new(),
                ProcessState::new(),
            ],
        }
    }

    pub unsafe fn process(
        &mut self,
        cpu_id: u8,
        mem_req: crossbeam_channel::Sender<crate::main_memory::Request>,
        code: &[u8],
    ) -> Option<crate::machine::CpuInst> {
        // find process to execute
        let proc = {
            let first_proc_to_check = self.last_proc + 1;
            let mut result = None;
            for i in first_proc_to_check..(first_proc_to_check + (NUM_PROCESSES as u8)) {
                let i = i % (NUM_PROCESSES as u8);

                if self.process_proc_ages[i as usize] == 0
                    && self.process_states[i as usize].state == self::process::State::Ready
                {
                    result = Some(i);
                    break;
                }
            }

            result
        };

        for val in &mut self.process_proc_ages {
            if *val > 0 {
                *val -= 1;
            }
        }

        match proc {
            Some(id) => {
                // keep this process from going into the pipe again!
                let id = id as usize;

                self.process_proc_ages[id] = 5;

                let proc = &mut self.process_states[id];

                // now do useful stuff lol

                // get instruction pointer
                let ip = *proc.registers.instruction_pointer();

                // decode instruction
                let raw_instruction = &code[ip as usize..ip as usize + 4];

                let instruction = Instruction::from_bytes(raw_instruction);

                // execute
                let (result,) = match instruction {
                    Instruction::R(op, dst, a, b, shift, func) => {
                        // TODO
                        (None,)
                    }
                    Instruction::I(op, dst, a, imm) => perform_i(op, dst, a, imm),
                };

                // perform memory access

                // update RIP and stuff
                {
                    *proc.registers.instruction_pointer() += 4;
                }

                result
            }

            // No process was ready to execute, will do nothing then!
            None => None,
        }
    }
}

unsafe fn perform_i(
    op: Operation,
    dst: RegId,
    a: RegId,
    imm: u16,
) -> (Option<crate::machine::CpuInst>,) {
    match op {
        Operation::Load => {
            use self::process::RegisterIndex;

            let unit: [u8; 2] = std::mem::transmute(imm);

            // proc.registers.write(RegisterIndex(dst, 0), &unit);

            (None,)
        }
        Operation::Cpu => match imm {
            0 => (Some(crate::machine::CpuInst::Halt),),
            _ => (None,),
        },
        _ => (None,),
    }
}

#[derive(Debug, Copy, Clone, Ord, PartialOrd, PartialEq, Eq)]
pub enum Operation {
    // 00000
    Load,

    // 10000
    Cpu,
}

pub type RegId = u8;
pub type ShiftAmount = u8;
pub type Func = u8;

#[derive(Debug, Copy, Clone, Ord, PartialOrd, PartialEq, Eq)]
pub enum Instruction {
    R(Operation, RegId, RegId, RegId, ShiftAmount, Func),
    I(Operation, RegId, RegId, u16),
}

#[derive(Copy, Clone, Ord, PartialOrd, PartialEq, Eq)]
pub enum InstructionType {
    R,
    I,
    J,
}

impl Instruction {
    pub fn from_bytes(bytes: &[u8]) -> Self {
        assert_eq!(bytes.len(), 4);

        let opcode = (bytes[0] & 0b1111_1100) >> 2;

        let (operation, ty) = {
            if opcode == 0b00_000000 {
                (Operation::Load, InstructionType::R)
            } else if opcode == 0b00_000001 {
                (Operation::Load, InstructionType::I)
            } else if opcode == 0b00_100000 {
                (Operation::Cpu, InstructionType::R)
            } else if opcode == 0b00_100001 {
                (Operation::Cpu, InstructionType::I)
            } else {
                unimplemented!()
            }
        };

        match ty {
            InstructionType::R => {
                let reg0 = ((bytes[0] & 0b0000_0011) << 2) | ((bytes[1] & 0b1110_0000) >> 5);
                let reg1 = (bytes[1] & 0b0001_1111);
                let reg2 = (bytes[2] & 0b1111_1000) >> 3;

                let shift_amount =
                    ((bytes[2] & 0b0000_0111) << 2) | ((bytes[3] & 0b1100_0000) >> 6);

                let func = (bytes[3] & 0b0011_1111);

                Instruction::R(operation, reg0, reg1, reg2, shift_amount, func)
            }
            InstructionType::I => {
                let reg0 = ((bytes[0] & 0b0000_0011) << 2) | ((bytes[1] & 0b1110_0000) >> 5);
                let reg1 = (bytes[1] & 0b0001_1111);

                let imm = ((bytes[2] as u16) << 8) | (bytes[3] as u16);

                Instruction::I(operation, reg0, reg1, imm)
            }
            InstructionType::J => unimplemented!(),
        }
    }
}

#[derive(Debug, Ord, PartialOrd, PartialEq, Eq)]
pub enum State {
    Running,
    Paused,
}
