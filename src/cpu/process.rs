use crate::memory::{MemoryAccess, Unit};

// in bytes
const PRIVATE_MEMORY_SIZE: usize = 32 * 1024;

// one register can fit 512 bits
//  - u8x64
//  - u16x32
//  - u32x16
//  - u64x8
//  - u128x4
const REG_SIZE: usize = 64;

#[derive(Debug, Copy, Clone)]
pub struct ProcessId(pub u8);

impl ProcessId {
    pub fn new(cpu: u8, proc: u8) -> Self {
        Self((cpu << 4) | (proc & 0x0F))
    }

    pub fn cpu(self) -> u8 {
        (self.0 & 0xF0) >> 4
    }

    pub fn procss(self) -> u8 {
        self.0 & 0x0F
    }
}

pub type Register = [u8; REG_SIZE];

#[derive(Debug, Copy, Clone, Ord, PartialOrd, PartialEq, Eq)]
pub enum State {
    Ready,
    WaitingForMemory,
    Inactive,
}

pub struct ProcessState {
    pub state: State,

    pub registers: RegisterFile,

    pub mem_read_buffer: crate::main_memory::MemoryLine,

    pub private_mem: PrivateMemory,
}

impl ProcessState {
    pub fn new() -> Self {
        ProcessState {
            state: State::Inactive,
            registers: RegisterFile::new(),
            private_mem: PrivateMemory::new(),
            mem_read_buffer: unsafe { std::mem::uninitialized() },
        }
    }
}

pub struct RegisterFile {
    pub regs: [Register; 31],
    // there is also a special NULL register which has no real storage
    // and discards everything that's written to it as well as yielding 0
    // when being read
}

impl RegisterFile {
    pub fn new() -> Self {
        RegisterFile {
            regs: unsafe { std::mem::uninitialized() },
        }
    }

    // instruction pointer is the lowest 64 bit in r31
    pub fn instruction_pointer(&mut self) -> &mut u64 {
        let ip_ref = &mut self.regs[30][0..8];

        let ip_ptr = unsafe { ip_ref.as_mut_ptr() };

        unsafe { &mut *(ip_ptr as *mut u64) }
    }
}

pub type RegisterId = u8;
pub type RegisterAccessOffset = u8;
pub struct RegisterIndex(pub RegisterId, pub RegisterAccessOffset);

static ZEROS: [u8; 64] = [0; 64];

impl MemoryAccess for RegisterFile {
    type Addr = RegisterIndex;

    unsafe fn read<U: Unit>(&self, addr: RegisterIndex, ret: &mut U) {
        let reg = addr.0;
        let offset = addr.1 as usize;

        let len = U::byte_length();

        if reg == 0 {
            ret.assign_slice(&ZEROS[0..len]);
            return;
        }

        let reg = reg - 1;

        ret.assign_slice(&self.regs[reg as usize][(offset * len)..(offset * len + len)]);
    }

    unsafe fn write<U: Unit>(&mut self, addr: RegisterIndex, val: &U) {
        let reg = addr.0;
        let offset = addr.1 as usize;

        let len = U::byte_length();

        if reg == 0 {
            // just discard the write
            return;
        }

        let reg = reg - 1;

        (&mut self.regs[reg as usize][(offset * len)..(offset * len + len)])
            .copy_from_slice(val.as_slice());
    }
}

pub struct PrivateMemory {
    pub contents: [u8; PRIVATE_MEMORY_SIZE],
}

impl PrivateMemory {
    pub fn new() -> Self {
        PrivateMemory {
            contents: unsafe { std::mem::uninitialized() },
        }
    }
}

// This implementation does **NOT** deal with alignment in any way.
impl MemoryAccess for PrivateMemory {
    type Addr = u16;

    unsafe fn read<U: Unit>(&self, addr: u16, ret: &mut U) {
        let addr = (addr & 0b0111_1111_1111_1111) as usize;

        let len = U::byte_length();

        ret.assign_slice(&self.contents[addr..(addr + len)]);
    }

    unsafe fn write<U: Unit>(&mut self, addr: u16, val: &U) {
        let addr = (addr & 0b0111_1111_1111_1111) as usize;

        let len = U::byte_length();

        (&mut self.contents[addr..(addr + len)]).copy_from_slice(val.as_slice());
    }
}

#[cfg(test)]
mod test {

    use super::*;
    use crate::memory::MemoryAccess;

    #[test]
    fn private_mem_access() {
        unsafe {
            let mut mem = PrivateMemory::new();

            mem.write(0, &[0, 1, 2, 3, 4, 5, 6, 7]);

            let mut res = [0; 8];

            mem.read(0, &mut res);

            assert_eq!(res, [0, 1, 2, 3, 4, 5, 6, 7]);
        }
    }

    #[test]
    fn register_file_access() {
        unsafe {
            let mut regs = RegisterFile::new();

            // test reg 0

            regs.write(RegisterIndex(0, 2), &[1, 3, 3, 7]);
            {
                let mut res = [0u8; 8];

                regs.read(RegisterIndex(0, 0), &mut res);

                assert_eq!(res, [0, 0, 0, 0, 0, 0, 0, 0]);
            }

            // test "real" registers

            regs.write(RegisterIndex(31, 0), &[1; 16]);
            regs.write(RegisterIndex(31, 1), &[2; 16]);

            {
                let mut res = [0u8; 4];
                regs.read(RegisterIndex(31, 5), &mut res);

                assert_eq!(res, [2, 2, 2, 2]);
            }

            regs.write(RegisterIndex(1, 0), &[4; 4]);
            regs.write(RegisterIndex(1, 1), &[2; 4]);

            {
                let mut res = [0u8; 8];
                regs.read(RegisterIndex(1, 0), &mut res);

                assert_eq!(res, [4, 4, 4, 4, 2, 2, 2, 2]);
            }
        }
    }

}
