# load number 0xFFFF_EEEE_DDDD_CCCC into r4

The pseudo instruction `li r0, 0xFFFF_EEEE_DDDD_CCCC` gets transformed into

```
lui r0, 0xCCCC
shli r0, r0, 8
lui r0, 0xDDDD
shli r0, r0, 8
lui r0, 0xEEEE
shli r0, r0, 8
lui r0, 0xFFFF
```


# load a byte from a loaded line

To load a byte from a loaded line, the `lbo` (load byte with offset)


// load the address into a register
li r8, $some_addr

// load memory into rline
ll r8

// load byte, offset
lbo rline, r8, 0

